const sha256 = require("sha256");
const currentNodeUrl = process.argv[3];
const uuid = require("uuid/v1");

class Blockchain {

    constructor(){
        this.chain = [];
        this.pendingTransactions = [];
        this.currentNodeUrl = currentNodeUrl;
        this.networkNodes = [];

        this.createNewBlock(100, '0', '0');
    }

    createNewBlock(nonce, previousBlockHash, hash){

        //Todo: create block class
        const newBlock = {
            index: this.chain.length + 1,
            timestamp: Date.now(),
            transactions: this.pendingTransactions,
            nonce,
            hash,
            previousBlockHash
        };

        this.pendingTransactions = [];
        this.chain.push(newBlock);

        return newBlock;
    }

    getLastBlock(){
        return this.chain[this.chain.length - 1]
    }

    createNewTransaction(amount, sender, recipient){
        const newTransaction = { 
            amount, 
            sender, 
            recipient,
            transactionId: uuid().split("-").join("")
        };

        return newTransaction;
    }

    addTransactionToPendingTransactions(transactionObj){
        this.pendingTransactions.push(transactionObj);
        return this.getLastBlock()['index'] + 1;
    }

    hashBlock(previousBlockHash, currentBlockData, nonce){
        const data = `${previousBlockHash}${JSON.stringify(currentBlockData)}${nonce}`;
        return sha256(data);
    }

    proofOfWork(previousBlockHash, currentBlockData){
         let nonce = 0;
         let hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);

         while (hash.substring(0,4) !== '0000') {
             nonce++; 
             hash = this.hashBlock(previousBlockHash, currentBlockData, nonce);
         }
         // nonce is the proof;
         return nonce;
    }

    chainIsValid(blockchain){
        // make sure hashes line up properly

        let validChain = true;

        for(var i = 1; i < blockchain.length; i++) {
            const currentBlock = blockchain[i];
            const previousBlock = blockchain[i - 1];
            const blockHash = 
            this.hashBlock(
                previousBlock.hash,
                { 
                    transactions: currentBlock.transactions,
                    index: currentBlock.index 
                }, 
                currentBlock.nonce
            )

            if(blockHash.substring(0,4) !== '0000') validChain = false
            if(currentBlock.previousBlockHash !== previousBlock.hash) validChain = false
        }

        const genesisBlock = blockchain[0];
        const correctNonce = genesisBlock.nonce === 100;
        const correctPreviousBlockHash = genesisBlock.previousBlockHash === '0';
        const correctHash = genesisBlock.hash === '0';
        const correctTransactions = genesisBlock.transactions.length === 0; 

        if(!correctNonce || !correctHash || !correctPreviousBlockHash || !correctTransactions) validChain = false

        return validChain
    }

    getBlock(blockHash) {
        let correctBlock = null;
        this.chain.forEach(block => {
            if(block.hash === blockHash) {
                correctBlock = block;
            }
        })
        return correctBlock;
    }

    getTransaction(transactionId){
        let correctTransaction = null;
        let correctBlock = null;
        this.chain.forEach(block => {
            block.transactions.forEach(transaction => {
                if(transaction.transactionId === transactionId) {
                    correctTransaction = transaction;
                    correctBlock = block;
                }
            })
        })

        return {
            transaction: correctTransaction,
            block: correctBlock
        }
    }

    getAddressData(address){
        const addressTransactions = [];
        this.chain.forEach(block =>  {
            block.transactions.forEach(transaction => {
                console.log("ADDRESS", address, "SENDER", transaction.sender, "RECIPIENT", transaction.recipient)
                if(transaction.sender === address || transaction.recipient === address) {
                    addressTransactions.push(transaction);
                }
            })
        })

        let balance = 0;
        addressTransactions.forEach(transaction => {
            if(transaction.recipient === address) balance += transaction.amount;
            else if (transaction.sender === address) balance -= transaction.amount;

            console.log(addressTransactions, balance)

        })
        return {
            addressTransactions,
            addressBalance: balance
        }
    }

}

module.exports = Blockchain;