const Blockchain = require("./blockchain");

const bitcoin = new Blockchain();

const bc1 = {
    "chain": [
      {
        "index": 1,
        "timestamp": 1528492508633,
        "transactions": [
          
        ],
        "nonce": 100,
        "hash": "0",
        "previousBlockHash": "0"
      },
      {
        "index": 2,
        "timestamp": 1528492642557,
        "transactions": [
          
        ],
        "nonce": 192133,
        "hash": "000073e7d35d35be0861f043cae54db47715fe0d145fd49bd5964140949f2a31",
        "previousBlockHash": "0"
      },
      {
        "index": 3,
        "timestamp": 1528492695884,
        "transactions": [
          {
            "amount": 12.5,
            "sender": "00",
            "recipient": "05b1ac906b6111e8997c290eb9d0d7a5",
            "transactionId": "5589bcd06b6111e8997c290eb9d0d7a5"
          },
          {
            "amount": 200,
            "sender": "sender",
            "recipient": "recipient",
            "transactionId": "682142506b6111e8997c290eb9d0d7a5"
          },
          {
            "amount": 20,
            "sender": "sender",
            "recipient": "recipient",
            "transactionId": "70340fe06b6111e8997c290eb9d0d7a5"
          },
          {
            "amount": 30,
            "sender": "sender",
            "recipient": "recipient",
            "transactionId": "723b51906b6111e8997c290eb9d0d7a5"
          },
          {
            "amount": 30,
            "sender": "sender",
            "recipient": "recipient",
            "transactionId": "72687c106b6111e8997c290eb9d0d7a5"
          }
        ],
        "nonce": 1651,
        "hash": "0000a0613912e57159840155931b5f110ea14acc751661acb8ca7872823ede15",
        "previousBlockHash": "000073e7d35d35be0861f043cae54db47715fe0d145fd49bd5964140949f2a31"
      },
      {
        "index": 4,
        "timestamp": 1528492753763,
        "transactions": [
          {
            "amount": 12.5,
            "sender": "00",
            "recipient": "05b1ac906b6111e8997c290eb9d0d7a5",
            "transactionId": "754e36e06b6111e8997c290eb9d0d7a5"
          },
          {
            "amount": 40,
            "sender": "sender",
            "recipient": "recipient",
            "transactionId": "85897ce06b6111e8997c290eb9d0d7a5"
          },
          {
            "amount": 50,
            "sender": "sender",
            "recipient": "recipient",
            "transactionId": "86fc46c06b6111e8997c290eb9d0d7a5"
          },
          {
            "amount": 60,
            "sender": "sender",
            "recipient": "recipient",
            "transactionId": "88f3a9f06b6111e8997c290eb9d0d7a5"
          }
        ],
        "nonce": 59919,
        "hash": "0000957bd6cadb8be84632278e4706334b600a1423489e740d664d1c69ad4c62",
        "previousBlockHash": "0000a0613912e57159840155931b5f110ea14acc751661acb8ca7872823ede15"
      },
      {
        "index": 5,
        "timestamp": 1528492764835,
        "transactions": [
          {
            "amount": 12.5,
            "sender": "00",
            "recipient": "05b1ac906b6111e8997c290eb9d0d7a5",
            "transactionId": "97cf87006b6111e8997c290eb9d0d7a5"
          }
        ],
        "nonce": 32723,
        "hash": "000094caa5970d1b854a677f7f95211b012fc959845f9ee63f172097938e07e1",
        "previousBlockHash": "0000957bd6cadb8be84632278e4706334b600a1423489e740d664d1c69ad4c62"
      },
      {
        "index": 6,
        "timestamp": 1528492766748,
        "transactions": [
          {
            "amount": 12.5,
            "sender": "00",
            "recipient": "05b1ac906b6111e8997c290eb9d0d7a5",
            "transactionId": "9e6774606b6111e8997c290eb9d0d7a5"
          }
        ],
        "nonce": 69110,
        "hash": "0000957c140973d202b4ffb592306bb4879dc4acd3e1ff3f20795bde0c09b107",
        "previousBlockHash": "000094caa5970d1b854a677f7f95211b012fc959845f9ee63f172097938e07e1"
      }
    ],
    "pendingTransactions": [
      {
        "amount": 12.5,
        "sender": "00",
        "recipient": "05b1ac906b6111e8997c290eb9d0d7a5",
        "transactionId": "9f8b33e06b6111e8997c290eb9d0d7a5"
      }
    ],
    "currentNodeUrl": "http://localhost:3001",
    "networkNodes": [
      
    ]
  }

  console.log("VALID: ", bitcoin.chainIsValid(bc1.chain))